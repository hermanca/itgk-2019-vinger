# Oppgave 4a)
def isPrimeNumber(n):
    if n <= 1:
        return False
    isPrime = True
    for i in range(2, n // 2 + 1):  # Holder å sjekke opp til og med n/2
        if n%i == 0:
            isPrime = False
            break
    return isPrime


# Oppgave 4b)
# Her er det flere muligheter. Vi vil vise en mulighet som bruker for-løkke og en som bruker while-løkke

# Mulighet 1: for-løkke
for i in range(2, 101):
    if isPrimeNumber(i):
        print(i)

# Mulighet 2: while-løkke
x = 2
while x < 101:
    if isPrimeNumber(x):
        print(x)
    x += 1
